package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;

import java.util.List;

public class TaskEndpointTest {
    @NonNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NonNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NonNull
    private static final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NonNull
    private static final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Nullable
    private static Session session;
    @Nullable Task task;
    private static String userLogin = "test";

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession(userLogin, userLogin);
    }

    @Before
    public void before() {
        task = new Task();
        task.setName("Task");
        task.setId("1");
        taskEndpoint.addTask(session, task);
    }

    @After
    public void after() {
        taskEndpoint.removeTask(session, task);
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NonNull final Task projectById = taskEndpoint.findByIdTask(session, task.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<Task> projects = taskEndpoint.findAllTask(session);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NonNull final List<Task> projects = taskEndpoint.findAllTaskByUserId(session);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final Task projects = taskEndpoint.findByIdTask(session, task.getId());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final Task projects = taskEndpoint.findByIdTask(session, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        taskEndpoint.removeByIdTask(session, task.getId());
        Assert.assertNull(taskEndpoint.findByIdTask(session, task.getId()));
    }
}

package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.List;


public class TaskServiceTest {
    @Nullable
    private TaskService taskService;
    @Nullable
    private TaskDTO task;
    private String userLogin = "test";

    @Before
    public void before() {
        taskService = new TaskService(new ConnectionService(new PropertyService()));
        taskService.add(new TaskDTO("Task"));
        task = taskService.findByName(userLogin, "Task");
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NonNull final TaskDTO projectById = taskService.findByName(userLogin, "Task");
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<TaskDTO> projects = taskService.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final TaskDTO projects = taskService.findByName(userLogin, task.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final TaskDTO projects = taskService.findByName(userLogin, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        taskService.remove(userLogin, task);
        Assert.assertNull(taskService.findByName(userLogin, "Task"));
    }

    @Test
    public void removeByErrorUserId() {
        taskService.remove("sd", task);
        @NonNull final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertEquals(0, tasks.size());
    }
}

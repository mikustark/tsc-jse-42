package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.component.Bootstrap;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AccessDeniedException;
import ru.tsc.karbainova.tm.exception.AccessForbiddenException;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.List;
import java.util.stream.Collectors;

public class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    public SessionService(IConnectionService connectionService) {
        super(connectionService);
    }


    @Override
    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        ServiceLocator serviceLocator = new Bootstrap();
        final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyUserNotFoundException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    public SessionDTO add(SessionDTO session) {
        if (session == null) throw new EmptySessionNlException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(
                    session.getId(),
                    session.getUserId(),
                    session.getSignature(),
                    session.getTimestamp()
            );
            sqlSession.commit();
            return session;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public SessionDTO open(String login, String password) {
//        final boolean check = checkDataAccess(login, password);
//        if (!check) throw new EmptyLoginOrPasswordException();
//        if (check) throw new EmptyLoginOrPasswordException();
        ServiceLocator serviceLocator = new Bootstrap();
        final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sign(session);
            add(session);
            sqlSession.commit();
            return session;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public SessionDTO sign(SessionDTO session) {
        if (session == null) throw new EmptySessionNlException();
        session.setSignature(null);
        IPropertyService propertyService = new PropertyService();
        final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<SessionDTO> getListSessionByUserId(String userId) {
        return findAll()
                .stream()
                .filter(s -> s.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void validate(SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NonNull final String signatureSource = session.getSignature();
        @NonNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (sessionRepository.exists(session.getId()).size() < 1) throw new AccessDeniedException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void validate(SessionDTO session, Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(session);
        final String userId = session.getUserId();
        ServiceLocator serviceLocator = new Bootstrap();
        final UserDTO user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessForbiddenException();
        if (user.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @Override
    @SneakyThrows
    public void close(SessionDTO session) {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(session.getId());
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void closeAll(SessionDTO session) {
        validate(session);
        List<SessionDTO> sessions = findAll().stream().filter(s -> s.getUserId().equals(session.getUserId())).collect(Collectors.toList());
        sessions.forEach(this::close);
    }

    @Override
    public void signOutByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<SessionDTO> sessions = findAll().stream().filter(s -> s.getUserId().equals(userId)).collect(Collectors.toList());
        sessions.forEach(this::close);
    }
}

package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IAdminUserService {

    void addAll(Collection<UserDTO> users);

    boolean isLoginExists(String login);

//    boolean isEmailExists(String email);

    List<UserDTO> findAll();

    UserDTO findByEmail(@NonNull String email);

    UserDTO findByLogin(String login);

    UserDTO removeUser(UserDTO user);

//    void removeByLogin(String login);

    UserDTO create(String login, String password);

    @SneakyThrows
    UserDTO create(@NonNull String login, @NonNull String password, @NonNull Role role);

    UserDTO create(String login, String password, String email);

//    User create(String login, String password, Role role);

    UserDTO setPassword(String userId, String password);

    UserDTO updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName);

    UserDTO lockUserByLogin(String login);

    UserDTO unlockUserByLogin(String login);

    @SneakyThrows
    void clear();

    @SneakyThrows
    UserDTO add(UserDTO user);
}

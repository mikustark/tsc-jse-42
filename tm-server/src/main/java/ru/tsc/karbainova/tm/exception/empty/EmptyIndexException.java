package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {
    public EmptyIndexException() {
        super("Error Index.");
    }
}

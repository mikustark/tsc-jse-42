package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.dto.AbstractOwnerDTOEntity;

public interface IOwnerService<E extends AbstractOwnerDTOEntity> extends IOwnerRepository<E> {

}

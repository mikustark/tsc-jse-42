package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService, ISaltSettings {
    @NonNull
    private static final String APP_VERSION_DEFAULT = "";
    @NonNull
    private static final String APP_VERSION_KEY = "version";
    @NonNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";
    @NonNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";
    @NonNull
    private static final String DEVELOPER_NAME_KEY = "developer";
    @NonNull
    private static final String DEVELOPER_NAME_DEFAULT = "";
    @NonNull
    private static final String FILE_NAME = "application.properties";
    @NonNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NonNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NonNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NonNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NonNull
    private final Properties properties = new Properties();
    @NonNull
    private static final String SIGNATURE_ITERATION_KEY = "session.iteration";
    @NonNull
    private static final String SIGNATURE_SECRET_KEY = "session.secret";
    @NonNull
    private static final String SIGNATURE_SECRET_KEY_DEFAULT = "";
    @NonNull
    private static final String HOST_KEY = "host";
    @NonNull
    private static final String HOST_KEY_DEFAULT = "";
    @NonNull
    private static final String PORT_KEY = "port";
    @NonNull
    private static final String PORT_KEY_DEFAULT = "";
    @NonNull
    private static final String JDBC_USER = "jdbc.user";
    @NonNull
    private static final String JDBC_PASSWORD = "jdbc.password";
    @NonNull
    private static final String JDBC_URL = "jdbc.url";
    @NonNull
    private static final String JDBC_DRIVER = "jdbc.driver";
    @NonNull
    private static final String DIALECT = "dialect";
    @NonNull
    private static final String AUTO = "auto";
    @NonNull
    private static final String SQLSHOW = "sqlshow";

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    private String getValue(String name, String defaultName) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getProperty(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultName);
    }

    @NonNull
    @Override
    public String getJdbcUser() {
        return getValue(JDBC_USER, "");
    }

    @NonNull
    @Override
    public String getJdbcPassword() {
        return getValue(JDBC_PASSWORD, "");
    }

    @NonNull
    @Override
    public String getJdbcUrl() {
        return getValue(JDBC_URL, "");
    }

    @NonNull
    @Override
    public String getJdbcDriver() {
        return getValue(JDBC_DRIVER, "");
    }

    @NonNull
    @Override
    public String getDialect() {
        return getValue(DIALECT, "");
    }

    @NonNull
    @Override
    public String getAuto() {
        return getValue(AUTO, "");
    }

    @NonNull
    @Override
    public String getSqlShow() {
        return getValue(SQLSHOW, "");
    }

    @NonNull
    @Override
    public String getApplicationVersion() {
        return getValue(APP_VERSION_KEY, APP_VERSION_DEFAULT);
    }

    @NonNull
    @Override
    public String getDeveloperName() {
        return getValue(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @NonNull
    @Override
    public String getDeveloperEmail() {
        return getValue(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NonNull String getServerHost() {
        return getValue(HOST_KEY, HOST_KEY_DEFAULT);
    }

    @Override
    public @NonNull String getServerPort() {
        if (System.getenv().containsKey(PORT_KEY)) {
            return System.getenv(PORT_KEY);
        }
        if (System.getProperties().containsKey(PORT_KEY)) {
            return System.getProperty(PORT_KEY);
        }
        return properties.getProperty(PORT_KEY, PORT_KEY_DEFAULT);
    }

    @NonNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT)
        );
    }

    @Override
    public @NonNull String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NonNull Integer getSignatureIteration() {
        if (System.getenv().containsKey(SIGNATURE_ITERATION_KEY)) {
            @NonNull final String value = System.getenv(SIGNATURE_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(SIGNATURE_ITERATION_KEY)) {
            @NonNull final String value = System.getProperty(SIGNATURE_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NonNull final String value = properties.getProperty(SIGNATURE_ITERATION_KEY);
        return Integer.parseInt(value);
    }

    @Override
    public @NonNull String getSignatureSecret() {
        if (System.getenv().containsKey(SIGNATURE_SECRET_KEY)) {
            return System.getenv(SIGNATURE_SECRET_KEY);
        }
        if (System.getProperties().containsKey(SIGNATURE_SECRET_KEY)) {
            return System.getProperty(SIGNATURE_SECRET_KEY);
        }
        return properties.getProperty(SIGNATURE_SECRET_KEY, SIGNATURE_SECRET_KEY_DEFAULT);
    }
}

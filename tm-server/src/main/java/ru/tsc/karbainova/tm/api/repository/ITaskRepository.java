package ru.tsc.karbainova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.Date;
import java.util.List;

public interface ITaskRepository {
    @Insert("INSERT INTO tm_task" +
            "(id, name, description, status, start_date, finish_date, created, user_id, project_id)" +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{start_date}, #{finish_date}, #{created}, #{user_id}), #{project_id}")
    void add(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("start_date") Date start_date,
            @Param("finish_date") Date finish_date,
            @Param("created") Date created,
            @Param("user_id") String user_id,
            @Param("project_id") String project_id
    );

    @Update("UPDATE tm_task" +
            "SET name=#{name}, description=#{description}, status=#{status}, start_date=#{start_date}, " +
            "finish_date=#{finish_date}, created=#{created}, user_id=#{user_id}, project_id=#{project_id} WHERE id = #{id}")
    void update(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("start_date") Date start_date,
            @Param("finish_date") Date finish_date,
            @Param("created") Date created,
            @Param("user_id") String user_id,
            @Param("project_id") String project_id
    );

    @Select("SELECT * FROM tm_task WHERE id=#{id} AND user_id=#{user_id} LIMIT 1")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    TaskDTO findByIdUserId(final String userId, final String id);

    @Select("SELECT * FROM tm_task WHERE name=#{name} AND user_id=#{user_id} LIMIT 1")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    TaskDTO findByNameUserId(final String userId, final String name);

    @Delete("SELECT * FROM tm_task WHERE user_id=#{user_id}")
    void clearByUserId(final String userId);

    @Delete("SELECT * FROM tm_task WHERE id=#{id} AND user_id=#{user_id} LIMIT 1")
    void removeByIdUserId(final String userId, final String id);

    @Select("SELECT * FROM tm_task WHERE user_id=#{user_id}")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    List<TaskDTO> findAllTaskByUserId(final String userId);

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    List<TaskDTO> findAll();

    @Delete("SELECT * FROM tm_task")
    void clear();
}

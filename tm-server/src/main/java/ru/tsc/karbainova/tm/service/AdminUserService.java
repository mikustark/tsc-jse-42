package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.api.service.IAdminUserService;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

public class AdminUserService extends AbstractService<UserDTO> implements IAdminUserService {

    public AdminUserService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public UserDTO removeUser(@Nullable final UserDTO user) {
        if (user == null) return null;
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeById(user.getId());
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO lockUserByLogin(@NonNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (user == null) return null;
            user.setLocked(true);
            userRepository.update(
                    user.getId(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getMiddleName(),
                    user.getEmail(),
                    user.getLogin(),
                    user.getRole().toString(),
                    user.getLocked(),
                    user.getPasswordHash()
            );
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO unlockUserByLogin(@NonNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (user == null) return null;
            user.setLocked(false);
            userRepository.update(
                    user.getId(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getMiddleName(),
                    user.getEmail(),
                    user.getLogin(),
                    user.getRole().toString(),
                    user.getLocked(),
                    user.getPasswordHash()
            );
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<UserDTO> collection) {
        if (collection == null) return;
        for (UserDTO i : collection) {
            add(i);
        }
    }

    public boolean isLoginExists(@NonNull final String login) {
        if (login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @SneakyThrows
    public UserDTO findByLogin(@NonNull final String login) {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO findByEmail(@NonNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByEmail(email);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            add(user);
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password, @NonNull final Role role) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            add(user);
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password, @NonNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            add(user);
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO setPassword(@NonNull final String userId, @NonNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        IPropertyService propertyService = new PropertyService();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = userRepository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            user.setPasswordHash(hash);
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @NonNull final String userId,
            @NonNull final String firstName,
            @NonNull final String lastName,
            @Nullable final String middleName) {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);

            UserDTO user = userRepository.findById(userId);
            if (user == null) throw new ProjectNotFoundException();
            userRepository.update(
                    user.getId(),
                    firstName,
                    lastName,
                    middleName,
                    user.getEmail(),
                    user.getLogin(),
                    user.getRole().toString(),
                    user.getLocked(),
                    user.getPasswordHash()
            );
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public UserDTO add(UserDTO user) {
        if (user == null) throw new EmptyUserNotFoundException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(
                    user.getId(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getMiddleName(),
                    user.getEmail(),
                    user.getLogin(),
                    user.getRole().toString(),
                    user.getLocked(),
                    user.getPasswordHash()
            );
            sqlSession.commit();
            return user;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}

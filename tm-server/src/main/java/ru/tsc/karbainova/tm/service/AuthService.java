package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.service.IAuthService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.IUserService;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyLoginException;
import ru.tsc.karbainova.tm.exception.empty.EmptyPasswordException;
import ru.tsc.karbainova.tm.exception.empty.EmptyRoleException;
import ru.tsc.karbainova.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;
    private final IPropertyService propertyService;

    private String userId;

    public AuthService(final IUserService userService, IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new EmptyIdException();
        return userId;
    }

    @Override
    public UserDTO getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        if (user.getLocked()) throw new EmptyRoleException();
        final String hash = HashUtil.salt((ISaltSettings) propertyService, password);
//        if (hash == null) throw new EmptyPasswordException();
        if (!hash.equals(user.getPasswordHash()) || user.getLocked()) throw new EmptyPasswordException();
        userId = user.getId();
    }

    @Override
    public void registry(@NonNull final String login, @NonNull final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

    @Override
    public void checkRole(final Role... roles) {
        if (roles == null || roles.length == 0) return;
        final UserDTO user = getUser();
        if (user == null) throw new EmptyRoleException();
        final Role role = user.getRole();
        if (role == null) throw new EmptyRoleException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new EmptyRoleException();
    }
}

package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.repository.IRepository;

import ru.tsc.karbainova.tm.dto.AbstractDTOEntity;

public interface IService<E extends AbstractDTOEntity> extends IRepository<E> {

//    void addAll(Collection<E> entities);
//
//    void clear();
//
//    void remove(E entity);
}

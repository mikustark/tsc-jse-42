package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @SneakyThrows
    List<TaskDTO> findAllTaskByUserId(String userId);

    void addAll(Collection<TaskDTO> tasks);

//    void create(String userId, String name);

    @SneakyThrows
    void clear();

    @SneakyThrows
    TaskDTO add(TaskDTO task);

    void create(String userId, String name, String description);

//    void add(String userId, Task task);

    void remove(String userId, TaskDTO task);

    List<TaskDTO> findAll();

//    List<Task> findAll(String userId);

//    List<Task> findAll(String userId, Comparator<Task> comparator);

    TaskDTO updateById(String userId, String id, String name, String description);

//    Task updateByIndex(String userId, Integer index, String name, String description);

//    void removeById(String userId, String id);

//    void removeByIndex(String userId, Integer index);
//
//    void removeByName(String userId, String name);

//    Task findById(String userId, String id);

//    Task findByIndex(String userId, Integer index);

    TaskDTO findByName(String userId, String name);

    @SneakyThrows
    TaskDTO findByIdUserId(@NonNull String userId, @NonNull String id);

//    Task startById(String userId, String id);
//
//    Task startByIndex(String userId, Integer index);
//
//    Task startByName(String userId, String name);
//
//    Task finishById(String userId, String id);
//
//    Task finishByIndex(String userId, Integer index);
//
//    Task finishByName(String userId, String name);

}
